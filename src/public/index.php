<?php
declare(strict_types=1); // strict mode

namespace  biblio;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
/*use scan\document\models\ArrayTag;
use scan\document\models\Document;
use scan\document\models\Tag;*/
use Slim\Factory\AppFactory;
/*use Slim\Routing\RouteContext;
use scan\framework\persistences\pdo\Connect;
use scan\framework\persistences\pdo\PersistenceDocument;
use scan\framework\persistences\pdo\PersistenceArrayDocument;*/

require __DIR__ . '/../vendor/autoload.php';

try{
    $app = AppFactory::create();

    $app->group('/api/1.0', function () use ($app) {
        $app->get('/', function (Request $request, Response $response, $args) {
            $response->getBody()->write("API v1.0 auto-biblio-slim");
            return $response;
        });

        $app->get('/documents/', function (Request $request, Response $response, $args) {
            return \biblio\controllers\documents\GetController::execute($request, $response);
        });

        $app->get('/documents/tag/{tag}', function (Request $request, Response $response, $args) {
            return \biblio\controllers\documents\tag\GetController::execute($request, $response);
        });

        $app->get('/documents/{id}', function (Request $request, Response $response, $args) {
            return \biblio\controllers\documents\GetByIdController::execute($request, $response);
        });

        $app->post('/documents/', function (Request $request, Response $response, $args) {
            return \biblio\controllers\documents\PostController::execute($request, $response);
        });

        $app->delete('/documents/{id}', function (Request $request, Response $response, $args) {
            return \biblio\controllers\documents\DeleteController::execute($request, $response);
        });
    });
    $app->run();
}
catch(\Exception $ex){
    print_r($ex);
}
