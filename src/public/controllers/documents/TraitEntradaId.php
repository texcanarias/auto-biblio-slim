<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents;

use  Slim\Psr7\Request;
use  Slim\Psr7\Response;
use Slim\Routing\RouteContext;

trait TraitEntradaId{
    private function entrada(Request $request) : int{
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();        
        $documentId = (int)$route->getArgument('id');
        if(0 === $documentId){
            throw new ExceptionInputNumericError();
        }
        return $documentId;
    }

}