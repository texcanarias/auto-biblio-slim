<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents;

use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Routing\RouteContext;
use \biblio\controllers\documents\exceptions\ExceptionInputNumericError;

class GetByIdController{

    use \biblio\controllers\documents\TraitEntradaId;

    public static function execute(Request $request, Response $response) : Response{
        $controller = new self();
        return $controller->core($request, $response);
    }

    private function core(Request $request, Response $response) : Response{
        try{
            $documentId = $this->entrada($request);
        } 
        catch(ExceptionInputNumericError $ex){
            $data = array('error' => 'input error is not numeric');
            $payload = json_encode($data);            
            $response->getBody()->write($payload);            
            $statusCode = 400;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);
        }

        //Negocio
        try{
            $document = \scan\document\services\GetByIdService::execute(
                                                        \scan\document\messages\GetByIdMessage::create($documentId) , 
                                                        \biblio\persistence\documents\PersistenceDocument::create());
        } catch (\Exception $ex){
            $statusCode = 500;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }
   
        return $this->salida($document, $response);
    }

    private function salida(\scan\document\models\Document $document,  Response $response) : Response{
        $payload = json_encode($document,JSON_UNESCAPED_SLASHES);

        $response->getBody()->write($payload);
        $statusCode = (null == $document->getId())?404:200;
        return $response
                        ->withHeader('Content-Type', 'application/json')
                        ->withStatus($statusCode);                
    }
}