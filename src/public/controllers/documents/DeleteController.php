<?php
declare (strict_types = 1); // strict mode
namespace biblio\controllers\documents;

use biblio\services\documents\DeleteService;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Routing\RouteContext;

class DeleteController
{

    use \biblio\controllers\documents\TraitEntradaId;

    public static function execute(Request $request, Response $response): Response
    {
        $controller = new self();
        return $controller->core($request, $response);
    }

    private function core(Request $request, Response $response): Response
    {
        try{
            $documentId = $this->entrada($request);
        } 
        catch(ExceptionInputNumericError $ex){
            $statusCode = 400;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }

        //Llamada a servicios
        try {
            $document = \scan\document\services\DeleteService::execute(
                \scan\document\messages\DeleteMessage::create($documentId),
                \biblio\persistence\documents\PersistenceDocument::create());
        } catch (\Exception $ex) {
            $statusCode = 500;
            return $response->withHeader('Content-Type', 'application/json')
                ->withStatus($statusCode);
        }

        return $this->salida($document, $response);
    }

    private function salida(\scan\document\models\Document $document, Response $response): Response
    {
        //Transformacion de datos
        $arrayDocument = ['id' => $document->getId(),
            'mime' => $document->getMime(),
            'name' => $document->getName(),
            'name_file' => $document->getNameFile(),
            'tags' => $document->getTags()];

        //Salida
        $payload = json_encode($arrayDocument);
        $response->getBody()->write($payload);
        $statusCode = (null == $document->getId()) ? 404 : 200;

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($statusCode);
    }
}
