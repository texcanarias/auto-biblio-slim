<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents;

use  Slim\Psr7\Request;
use  Slim\Psr7\Response;

class GetController{

    use \biblio\controllers\documents\TraitSalidaArrayDocument;

    public static function execute(Request $request, Response $response) : Response
    {
        $controller = new self();
        return $controller->core($request, $response);
    }

    private function core(Request $request, Response $response) : Response{
        try{
            $documents = \scan\document\services\GetService::execute(\biblio\persistence\documents\PersistenceArrayDocument::create());
        } catch (\Exception $ex){
            $statusCode = 500;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }

        
        return $this->salida($documents, $response);
    }
}