<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents;

use  Slim\Psr7\Request;
use  Slim\Psr7\Response;

trait TraitSalidaArrayDocument{
    private function salida(\scan\document\models\ArrayDocument $documents,  Response $response) : Response{
        $collection = [];
        foreach($documents as $document){
            //Transformacion de datos
            $collection[] = [  'id' => $document->getId(),
                                'mime' =>  $document->getMime(),
                                'name' => $document->getName(),
                                'name_file' => $document->getNameFile(),
                                'tags' => $document->getTags()];
        }

        //Salida
        $payload = json_encode($collection);
        $response->getBody()->write($payload);
        return $response
                        ->withHeader('Content-Type', 'application/json')
                        ->withStatus(200);
    }

}