<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents;

use  Slim\Psr7\Request;
use  Slim\Psr7\Response;
use scan\document\models\{Document,Tag,ArrayTag};
use \biblio\controllers\documents\exceptions\{ExceptionInputMimeRequired,ExceptionInputNameRequired,ExceptionInputNameFileRequired};

class PostController{
    public static function execute(Request $request, Response $response) : Response{
        $controller = new self();
        return $controller->core($request, $response);
    }

    private function core(Request $request, Response $response) : Response
    {
        try{
            $document = $this->entrada($request);
        } 
        catch(ExceptionInputMimeRequired $ex){
            $data = array('error' => 'input mime is required');
            $payload = json_encode($data);            
            $response->getBody()->write($payload);            
            $statusCode = 400;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }
        catch(ExceptionInputNameRequired $ex){
            $data = array('error' => 'input name is required');
            $payload = json_encode($data);            
            $response->getBody()->write($payload);            
            $statusCode = 400;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }
        catch(ExceptionInputNameFileRequired $ex){
            $data = array('error' => 'input name file is required');
            $payload = json_encode($data);            
            $response->getBody()->write($payload);            
            $statusCode = 400;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }
        catch(\Exception $ex){
            $data = array('error' => $ex->getMessage());
            $payload = json_encode($data);            
            $response->getBody()->write($payload);            
            $statusCode = 400;
            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($statusCode);    
        }

        //Negocio
        try{
            $document = \scan\document\services\PostService::execute(
                                                \scan\document\messages\PostMessage::create($document), 
                                                \biblio\persistence\documents\PersistenceDocument::create());
        } catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return $this->salida($document, $response);
    }

    private function entrada(Request $request) : \scan\document\models\Document{
        // Get all POST parameters
        $json = $request->getBody();
        $data = json_decode((string)$json, true);
        if(JSON_ERROR_NONE !== json_last_error ( )){
            throw new \Exception("Json error " . json_last_error_msg());
        }

        // Get a single POST parameter
        $mime = $data['mime'];
        if(null == $mime){
            echo "*1";
            throw new ExceptionInputMimeRequired;
        }

        $name = $data['name'];
        if(null == $name){
            echo "*2";
            throw new ExceptionInputNameRequired;
        }

        $nameFile = $data['name_file'];
        if(null == $nameFile){
            echo "*3";
            throw new ExceptionInputNameFileRequired;
        }

        $tags = $data['tags'];
        if(null == $tags){
            $tags = "";
        }

        $docTags = new ArrayTag();
        foreach($tags as $tag){
            $docTags->add(Tag::factoryNew(null,$tag));
        }

        try{
            $document = Document::factoryFromArrayTag(null,$name,$nameFile,$mime,$docTags);
        } catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return $document;
    }

    private function salida(\scan\document\models\Document $document,  Response $response) : Response{
        $payload = json_encode($document);
        $response->getBody()->write($payload);
        return $response
                        ->withHeader('Content-Type', 'application/json')
                        ->withStatus(200);
    }
}