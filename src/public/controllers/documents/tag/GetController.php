<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents\tag;

use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Routing\RouteContext;

class GetController{

    use \biblio\controllers\documents\TraitSalidaArrayDocument;

    public static function execute(Request $request, Response $response) : Response
    {
        $controller = new self();
        return $controller->core($request, $response);
    }

    private function core(Request $request, Response $response): Response       
    {
        $tag = $this->entrada($request);
 
        try{
            $documents = \biblio\services\documents\tag\GetService::execute(
                                                                            \biblio\messages\documents\GetMessagesByTag::create($tag) , 
                                                                            \biblio\persistence\documents\PersistenceArrayDocument::create()
                                                                        );
        }
        catch(\Exception $ex){
            //TODO Devolver error 500
            throw $ex;
        }

        return $this->salida($documents, $response); 
        }

        private function entrada(Request $request): string
        {
            $routeContext = RouteContext::fromRequest($request);
            $route = $routeContext->getRoute();        
            $tag = $route->getArgument('tag');
            return $tag;
        }
   
}