<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents\exceptions;

class ExceptionInputNumericError extends \Exception{
    public function __construct(){
        return parent::__construct("Numeric error");
    }
}