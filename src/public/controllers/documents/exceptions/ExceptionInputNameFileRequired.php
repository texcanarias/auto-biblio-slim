<?php
declare(strict_types=1); // strict mode
namespace biblio\controllers\documents\exceptions;

class ExceptionInputNameFileRequired extends \Exception{
    public function __construct(){
        return parent::__construct("Name file required");
    }
}