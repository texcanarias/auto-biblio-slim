<?php
declare(strict_types=1); // strict modecd
namespace biblio\controllers\documents\exceptions;

class ExceptionInputMimeRequired extends \Exception{
    public function __construct(){
        return parent::__construct("Mime required");
    }
}