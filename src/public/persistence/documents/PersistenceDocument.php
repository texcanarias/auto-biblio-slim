<?php
declare(strict_types=1); // strict mode
namespace biblio\persistence\documents;

use scan\document\persistences\InterfacePersistenceDocument;
use scan\framework\persistences\pdo\PersistenceDocument as PerPersistenceDocument;

class PersistenceDocument{
    public static function create() : InterfacePersistenceDocument{
        $per = new PerPersistenceDocument(Connect::create());
        return $per;
    }
}