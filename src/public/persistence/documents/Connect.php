<?php
declare(strict_types=1); // strict mode
namespace biblio\persistence\documents;

use scan\framework\persistences\pdo\Connect as PerConnect;

class Connect{
    public static function create(){
        $connect = new PerConnect('db_auto_biblio_slim','auto_biblio','root','root');
        return $connect;
    }
}