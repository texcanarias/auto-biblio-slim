<?php
declare(strict_types=1); // strict mode
namespace biblio\persistence\documents;

use scan\document\persistences\InterfacePersistenceArrayDocument;
use scan\framework\persistences\pdo\PersistenceArrayDocument as PerPersistenceArrayDocument;

class PersistenceArrayDocument{
    public static function create() : InterfacePersistenceArrayDocument{
        $per = new PerPersistenceArrayDocument(Connect::create());
        return $per;
    }
}