<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class RestTest extends TestCase
{
    public function testType()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://localhost/',
            // You can set any number of default request options.
            'timeout'  => 2.0
        ]);

        //Alta del registro
        $body = [];
        $body['mime'] = "mime/png";
        $body['name'] = "Documento principal";
        $body['name_file'] = "doc.txt";
        $body['tags'] = json_encode(["test","unitario"]);

        $response = $client->post('documents/', ['form_params' => $body]);
        $code = $response->getStatusCode();
        $body = $response->getBody();
        $jsonBody = json_decode($body);
        $this->assertEquals(200, $code);

        //Recuperación del último registro
        $response = $client->request('GET', 'documents/' . $jsonBody->id);
        $code = $response->getStatusCode();
        $body = $response->getBody();
        $this->assertEquals(200, $code);

        //Recuperaciín del listado completo
        $response = $client->request('GET', 'documents/');
        $code = $response->getStatusCode();
        $body = $response->getBody();
        $this->assertEquals(200, $code);

        //Borrado del registro
        $response = $client->request('DELETE', 'documents/' . $jsonBody->id);
        $code = $response->getStatusCode();
        $body = $response->getBody();
        $this->assertEquals(200, $code);
    }
}